pragma solidity ^0.8.20;

contract SmartAuction {
    address payable public owner;
    string public lotNumber;
    uint256 public price;
    address payable public seller;
    address payable public buyer;
    address payable public auctioneer;
    uint256 start;
    uint256 end;
    bool public active;
    uint public highestBid = 0;
    address payable public highestBidder;

    constructor(string memory number_lot, uint price_to, address payable seller_to, address payable auctioneer_to, uint biddingTime_to) {
        lotNumber = number_lot;
        active = true;
        price = price_to;
        owner = seller_to;
        seller = seller_to;
        auctioneer = auctioneer_to;
        start = block.timestamp;
        end = block.timestamp + biddingTime_to;
    }

    function bid() public payable {
        require(block.timestamp <= end, "Auction has ended");
        require(msg.value > price, "Bid not high enough");

        if (highestBid != 0) {
            highestBidder.transfer(highestBid); // Rimborso all'offerente precedente
        }

        highestBidder = payable(msg.sender);
        highestBid = msg.value;
    }

    function endAuction() public {
        require(
            msg.sender == auctioneer,
            "Only auctioneer can end the auction"
        );
        require(block.timestamp >= end, "Auction has not ended yet");
        require(active, "Auction already ended");

        active = false;

        seller.transfer(highestBid); // Trasferisci i soldi all'astaio
        owner = highestBidder;

    }
}
